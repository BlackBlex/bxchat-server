/**
 * BXChat Server
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package app
 *
 * ==============Information==============
 *      Filename: Startup.java
 * ---------------------------------------
*/

package app;

import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketUsername;
import com.blackblex.plugins.core.Core;
import com.blackblex.plugins.core.GlobalService;
import commands.Disconnect;
import commands.Online;
import commands.SendMessage;
import frames.ServerFrame;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JFrame;

public class Startup
{

    public static Map<Integer, SocketUsername> clientsOnline = new LinkedHashMap<>();
    public static Map<String, CommandInterface> commandList = new LinkedHashMap<>();
    public static boolean closeServer = false;

    public static int maxClients = 3;

    public static Core core;

    public Startup()
    {
        try
        {
    		JFrame.setDefaultLookAndFeelDecorated(true);
    		JDialog.setDefaultLookAndFeelDecorated(true);
            javax.swing.UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        }
        catch ( ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex )
        {
            System.out.println("[Main]: " + ex.getMessage());
        }

        new ServerFrame().setVisible(true);

        GlobalService global = new GlobalService();
        core = new Core("command");
        core.setGlobalService(global);

        Core.globalService.addObject("CLIENTSONLINE", clientsOnline);
        Core.globalService.addObject("COMMANDLIST", commandList);
        core.exec();

        //#########Command list#########
	        commandList.put("send", new SendMessage());
	        commandList.put("online", new Online());
	        commandList.put("disconnect", new Disconnect());
        //#########Command list#########

        ServerSocket serverSocket;
        ServerFrame.jtextAreaConsole.append("Inicializando servidor... ");
        try
        {
            serverSocket = new ServerSocket(345);
            ServerFrame.jtextAreaConsole.append("\t[OK]\n");
            ServerFrame.jtextAreaConsole.append("Esperando conexi�n... \n");
            int idSession = 0;
            while ( !closeServer )
            {
                Socket incoming = serverSocket.accept();
                ServerFrame.jtextAreaConsole.append("Nueva conexi�n: " + incoming + "\n");
                SocketUsername socketNew = new SocketUsername();
                socketNew.setSocketClient(incoming);
                socketNew.setIdSession(idSession);
                ServerManager sm = new ServerManager(socketNew);
                sm.start();
                idSession++;
            }
        }
        catch ( IOException e )
        {
            System.out.println("[Main #2]: " + e.getMessage());
        }
    }
}

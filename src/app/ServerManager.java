/**
 * BXChat Server
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package app
 *
 * ==============Information==============
 *      Filename: ServerManager.java
 * ---------------------------------------
*/

package app;

import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;

import frames.ServerFrame;

import java.io.IOException;
import java.io.ObjectInputStream;
import javax.swing.text.BadLocationException;

public class ServerManager extends Thread
{

    private final SocketUsername socketUsername;
    private ObjectInputStream objectInput;
    private SocketMessage dataInput = null, dataOutput = null;

    public ServerManager(SocketUsername socket)
    {
        if ( !Startup.clientsOnline.containsValue(socket) )
        {
            Startup.clientsOnline.put(socket.getIdSession(), socket);
        }

        this.socketUsername = socket;

    }

    public SocketMessage receive()
    {
        SocketMessage data = null;
        try
        {
            if ( !this.socketUsername.getSocketClient().isClosed() )
            {
                this.objectInput = new ObjectInputStream(this.socketUsername.getSocketClient().getInputStream());
                data = (SocketMessage) this.objectInput.readObject();
            }
        }
        catch ( IOException | ClassNotFoundException ex )
        {
        	ex.printStackTrace();
            System.out.println("[ServerManager receive]: " + ex.getMessage());
        }
        return data;
    }

    @Override
    public void run()
    {
        while ( !this.isInterrupted() )
        {

            ServerFrame.jlabelClientsOnline.setText("<html>Clientes conectados: <b>" + Startup.clientsOnline.size() + "</b></html>");

            this.dataInput = receive();
            if ( this.dataInput != null )
            {
                printData(this.dataInput);

                if ( this.socketUsername.isNewsocket() )
                {
                    this.socketUsername.setNewSocket(false);
                    this.dataOutput = new SocketMessage();
                    this.dataOutput.setAction("register");
                    this.dataOutput.setFrom(-1);
                    this.dataOutput.setTo(-1);
                    this.dataOutput.setMessage(Integer.toString(this.socketUsername.getIdSession()));
                    this.socketUsername.sendClient(this.dataOutput);
                }

                if ( !this.dataInput.getAction().isEmpty() )
                {
                    if ( Startup.commandList.containsKey(this.dataInput.getAction()) )
                    {
                        CommandInterface command = Startup.commandList.get(this.dataInput.getAction());
                        if ( command.canExecute(this.socketUsername) )
                        {
                            command.execute(this.socketUsername, this.dataInput);
                        }
                    }
                }
            }
        }
    }

    public void printData(SocketMessage data)
    {
        ServerFrame.jtextAreaConsole.append("[" + data.getFrom() + " -> " + data.getTo() + "] " + data.getAction());

        if ( data.getMessage() != null )
        {
            ServerFrame.jtextAreaConsole.append(" - " + data.getMessage());
        }

        if ( data.getjMessage() != null )
        {
            try
            {
                ServerFrame.jtextAreaConsole.append(" - " + data.getjMessage().getText(0, data.getjMessage().getLength()));
            }
            catch ( BadLocationException ex )
            {
            }
        }
        ServerFrame.jtextAreaConsole.append("\n");
    }

}

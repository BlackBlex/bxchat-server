/**
 * BXChat Server
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package frames
 *
 * ==============Information==============
 *      Filename: ServerFrame.java
 * ---------------------------------------
*/

package frames;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.event.WindowAdapter;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.text.DefaultCaret;

import com.blackblex.libs.application.components.borders.RoundedSidesBorder;

import app.Startup;

public class ServerFrame extends Frame
{
	private static final long serialVersionUID = 3617013234965620143L;
	public static JTextArea jtextAreaConsole;
	private JScrollPane jscrollPane;
	private JSlider jsliderMaxClients;
	private JLabel jlabelMaxClients;
	public static JLabel jlabelClientsOnline;

	public ServerFrame()
	{
		super();

		super.FRAME_WIDTH = (int) (super.FRAME_WIDTH*0.75);

		finishInit();
	}

	@Override
	public void beforeInit()
	{
		super.beforeInit();
		jtextAreaConsole = new JTextArea();
		jtextAreaConsole.setLineWrap(true);
		jtextAreaConsole.setWrapStyleWord(true);
		jtextAreaConsole.setEditable(false);

		DefaultCaret caret = (DefaultCaret) jtextAreaConsole.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		jscrollPane = new JScrollPane(jtextAreaConsole);
		jscrollPane.setBackground(Color.WHITE);
		jscrollPane.setBorder(BorderFactory.createCompoundBorder(new RoundedSidesBorder(Color.darkGray, 3, 20, 20),
				BorderFactory.createEmptyBorder(10, 10, 10, 10)));

		jsliderMaxClients = new JSlider();
		jsliderMaxClients.setMaximum(50);
		jsliderMaxClients.setValue(Startup.maxClients);
		jsliderMaxClients.setBackground(Color.WHITE);

		jlabelMaxClients = new JLabel("<html>Clientes maximos: <b>" + Startup.maxClients + "</b></html>");

		jlabelClientsOnline = new JLabel("<html>Clientes conectados: <b>" + Startup.clientsOnline.size() + "</b></html>");
		jlabelClientsOnline.setFont(new Font(jlabelClientsOnline.getFont().getFontName(), Font.PLAIN, 16));
		jlabelClientsOnline.setHorizontalAlignment(SwingConstants.CENTER);

		super.jpanelBottom.setMaximumSize(new Dimension(super.FRAME_WIDTH,(int) (super.FRAME_HEIGHT*0.10)));
		super.jpanelBottom.setPreferredSize(new Dimension(super.FRAME_WIDTH,(int) (super.FRAME_HEIGHT*0.10)));
		super.jpanelBottom.setMinimumSize(new Dimension(super.FRAME_WIDTH,(int) (super.FRAME_HEIGHT*0.10)));
	}

	@Override
	public void init()
	{
		super.init();

		super.jpanelHeader.add(jlabelClientsOnline);
		super.gridbag.weightx = 1;
		super.gridbag.weighty = 1;
		super.gridbag.fill = GridBagConstraints.BOTH;
		super.jpanelBody.add(jscrollPane, super.gridbag);
		super.gridbag.weighty = 0;
		super.jpanelBottom.add(jlabelMaxClients, super.gridbag);
		super.jpanelBottom.add(this.jsliderMaxClients, super.gridbag);
		this.pack();
	}

	@Override
	public void afterInit()
	{
		this.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(java.awt.event.WindowEvent evt)
			{
				Startup.core.closePlugins();
			}
		});

		jsliderMaxClients.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
            	Startup.maxClients = jsliderMaxClients.getValue();

            	jlabelMaxClients.setText("<html>Clientes maximos: <b>" + Startup.maxClients + "</b></html>");
            }
        });

	}

	@Override
	public void finishInit()
	{
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.jpanelHeader.setBackground(Color.WHITE);
		super.jpanelBody.setBackground(Color.WHITE);
		super.jpanelBottom.setBackground(Color.WHITE);

		super.showFrame("BXChat", "Servidor");
	}

}

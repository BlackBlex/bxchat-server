/**
 * BXChat Server
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package commands
 *
 * ==============Information==============
 *      Filename: Disconnect.java
 * ---------------------------------------
*/

package commands;

import java.io.IOException;
import java.util.Map;

import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;

import app.Startup;

public class Disconnect implements CommandInterface
{

    @Override
    public String getDescription()
    {
        return "Envia la notificaci�n de que un usuario se ha desconectado a los demas usuarios";
    }

    @Override
    public boolean canExecute(SocketUsername socketUsername)
    {
        return true;
    }

    @Override
    public void execute(SocketUsername socketUsername, SocketMessage dataInput)
    {
        for ( Map.Entry<Integer, SocketUsername> client : Startup.clientsOnline.entrySet() )
        {
            if ( dataInput.getTo() == -1 && client.getKey() != dataInput.getFrom() )
            {
                SocketMessage dataOutput = new SocketMessage();
                dataOutput.setAction("disconnect");
                dataOutput.setFrom(dataInput.getFrom());
                dataOutput.setTo(-1);
                dataOutput.setMessage(dataInput.getMessage());
                client.getValue().sendClient(dataOutput);
            }
        }
        Startup.clientsOnline.remove(dataInput.getFrom());
        try
        {
            socketUsername.getSocketClient().close();
        }
        catch ( IOException e )
        {
            System.out.println("[Disconnect]: " + e.getMessage());
        }
    }

}

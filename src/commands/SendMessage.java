/**
 * BXChat Server
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package commands
 *
 * ==============Information==============
 *      Filename: SendMessage.java
 * ---------------------------------------
*/

package commands;

import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;
import com.blackblex.plugins.core.Core;
import java.util.Map;

public class SendMessage implements CommandInterface
{

    @Override
    public String getDescription()
    {
        return "Manda un mensaje a todos los usuarios";
    }

    @Override
    public boolean canExecute(SocketUsername socketUsername)
    {
        return true;
    }

    @Override
    public void execute(SocketUsername socketUsername, SocketMessage dataInput)
    {
        Map<Integer, SocketUsername> clients = (Map<Integer, SocketUsername>) Core.globalService.getObject("CLIENTSONLINE");

        for ( Map.Entry<Integer, SocketUsername> client : clients.entrySet() )
        {
            if ( client.getKey() != dataInput.getFrom() )
            {
                client.getValue().sendClient(dataInput);
            }
        }

    }

}

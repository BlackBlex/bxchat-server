**BXChat Server**
===================


Es un chat escrito en java que contiene un sistema de plugins que complementerá el sistema.


*Requiere:* [BlexLib](https://bitbucket.org/BlackBlex/blexlib)

--------
**Caracteristicas:**

 - Enviar mensajes a todos los usuarios conectados.
 - Sistema de plugins.

--------
**Plugins disponibles:**

 - **SendPMMessage** ~ Para que identifique el servidor cuando se trata de un mensaje privado | Complemento de SendPMMessage - [BXChat Client](https://bitbucket.org/BlackBlex/bxchat-client)
 - **SendFile** ~ Para que identifique el servidor cuando se trata de un envió de archivo | Complemento de SendFile - [BXChat Client](https://bitbucket.org/BlackBlex/bxchat-client)

--------

**Nota:** *Se irá añadiendo más características conforme se vaya avanzando en su desarrollo. **No cuenta con sistema de seguridad, se implementará más adelante***



--------
 BXChat | Chat básico con sistema de plugins

 Servidor escrito en java

 Author: @BlackBlex (BlackBlex)

 License: General Public License (GPLv3) | http://www.gnu.org/licenses/